'use strict'

var express = require('express');
var fedex = require('../fedex');
var router = express.Router();


router.get('/test', function(req, res) {
    return res.status(200).send({message: 'Hellow World', author: 'Carlos Marin'})
});

router.get('/tracking/:number', function(req, res) {
	var number = req.params.number;
	console.log('TRACKING_NUMBER => ', number)
	fedex.track({
  	 SelectionDetails: {
    	PackageIdentifier: {
      		Type: 'TRACKING_NUMBER_OR_DOORTAG',
      		Value: number
    	}
  	 }
	}, function(err, result) {
  		if(err) { 
  			console.log(err);
  			return res.status(500).send({data: err});
  		}
		console.log('success 200')
    	return res.status(200).send(result);
	});
});

module.exports = router;